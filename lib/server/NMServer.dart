import 'package:chopper/chopper.dart';
import "dart:async";
part 'NMServer.chopper.dart';

@ChopperApi(baseUrl: "/nixmix/")
abstract class PostApiService extends ChopperService {
  @Post(path: 'notify/', headers: {contentTypeKey:jsonHeaders})
  Future<Response> postPost(@Body() Map bodyRequest,);

  @Get(path : 'getConditions/')
  Future<Response> getConditions();


  static PostApiService create() {
    final client = ChopperClient(baseUrl: 'http://127.0.0.1:5000',
    services: [
      _$PostApiService(),
    ],
      converter: JsonConverter(),
    );
    return _$PostApiService(client);
  }
}