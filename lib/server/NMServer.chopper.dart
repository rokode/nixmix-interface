// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'NMServer.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$PostApiService extends PostApiService {
  _$PostApiService([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = PostApiService;

  @override
  Future<Response<dynamic>> postPost(Map<dynamic, dynamic> bodyRequest) {
    final $url = '/nixmix/notify/';
    final $headers = {'content-type': 'application/json'};
    final $body = bodyRequest;
    final $request =
        Request('POST', $url, client.baseUrl, body: $body, headers: $headers);
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response<dynamic>> getConditions() {
    final $url = '/nixmix/getConditions/';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }
}
