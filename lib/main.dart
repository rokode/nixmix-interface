import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:nixmix_interface/server/NMServer.dart';
import 'package:provider/provider.dart';
import 'Configuration.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider(
      dispose: (_, PostApiService service) => service.client.dispose(),
      create: (_) =>  PostApiService.create(),
      child: MaterialApp(
      title: 'NixMix',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'NixMix-beta'),
      ),
    );
  }

  static void showAlertDialog(context,title, message, btnText) {
    Widget okButton = FlatButton(
      child: Text(btnText),
      onPressed: () {
        Navigator.of(context).pop(); // dismiss dialog
      },
    );
    AlertDialog alert = AlertDialog(
      title: Text(title),
      content: Text(message),
      actions: [
        okButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  static void notifyAction(context, newAction) async {
    final response = await Provider.of<PostApiService>(context,listen: false).postPost(buildBodyJsonResponse("start"));
    if(response.isSuccessful){
      Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Configuration()));
    }else{
      showAlertDialog(context, "Error", "NixMix Server connection.", "Ok");
    }
    return;
  }
  static Map buildBodyJsonResponse(String newAction) {
    Map data = (
        {
          'currentAction': newAction,
          'currentStatus': 'started',
          'date': '',
          'destination': 'nixMix',
          'origin': 'appLocal',
          'protocol_version': 1,
          'ttl': 60000
        }
    );
    return data;
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
//      appBar: AppBar(
//        title: Text(widget.title),
//      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Spacer(flex: 15),
            Text(
              'NIXMIX BETA',
              style: TextStyle(fontSize: 25,fontWeight: FontWeight.bold), textAlign: TextAlign.center,
            ),
            Spacer(flex: 5),
            Padding(
              padding: const EdgeInsets.only(left: 150.0, top: 10.0, bottom: 10.0, right: 150.0),
              child:
              Text(
                'The ancient method of nixtamalization meets the most inovative technologies, for you to have the beast quality masa in your home or business.\n',
                style: TextStyle(fontSize: 20), textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 150.0, top: 10.0, bottom: 50.0, right: 150.0),
              child:
              Text(
                'By choosing preset reciepes or setting your own, make masa with the precise properties that your dish requires.',
                style: TextStyle(fontSize: 20), textAlign: TextAlign.center,
              ),
            ),
            Spacer(flex: 5),
            RaisedButton(
              padding: const EdgeInsets.only(
                left: 90,
                top: 20,
                right: 90,
                bottom: 20,
              ),
              textColor: Colors.white,
              color: Colors.black,

              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0),
                  side: BorderSide(color: Colors.black)
              ),
              onPressed: () {
                MyApp.notifyAction(context,"start");
              },
              child: const Text('START MIXING', style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold)),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 200.0, top: 10.0, bottom: 10.0, right: 200.0),
              child:
              Image.asset('assets/images/minerva_transp.png'),
            ),
          ],
        )
      ),

    );
  }
}
