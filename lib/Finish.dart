import 'package:flutter/material.dart';
import 'package:nixmix_interface/server/NMServer.dart';
import 'package:provider/provider.dart';

import 'main.dart';

class Finish extends MyApp {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
//      appBar: AppBar(
//        title: Text(widget.title),
//      ),
      body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Spacer(flex: 15),
              Text(
                'MASA IS READY!',
                style: TextStyle(fontSize: 25,fontWeight: FontWeight.bold), textAlign: TextAlign.center,
              ),
              Spacer(flex: 5),
              Padding(
                padding: const EdgeInsets.only(left: 150.0, top: 10.0, bottom: 10.0, right: 150.0),
                child:
                Text(
                  'The ancient method of nixtamalization meets the most inovative technologies, for you to have the beast quality masa in your home or business.\n',
                  style: TextStyle(fontSize: 20), textAlign: TextAlign.center,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 150.0, top: 10.0, bottom: 50.0, right: 150.0),
                child:
                Text(
                  'By choosing preset reciepes or setting your own, make masa with the precise properties that your dish requires.',
                  style: TextStyle(fontSize: 20), textAlign: TextAlign.center,
                ),
              ),
              Spacer(flex: 5),
              RaisedButton(
                padding: const EdgeInsets.only(
                  left: 90,
                  top: 20,
                  right: 90,
                  bottom: 20,
                ),
                textColor: Colors.white,
                color: Colors.black,

                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(30.0),
                    side: BorderSide(color: Colors.black)
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => MyApp()));
                },
                child: const Text('FINISH', style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold)),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 200.0, top: 10.0, bottom: 10.0, right: 200.0),
                child:
                Image.asset('assets/images/minerva_transp.png'),
              ),
            ],
          )
      ),
    );
  }

  void onLoading(context) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          elevation: 0,
          backgroundColor: Colors.transparent,
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              new CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
                backgroundColor: Colors.transparent,
              ),
            ],
          ),
        );
      },
    );
  }

  Future<String> notifyStatus(context) async {
    var response = await Provider.of<PostApiService>(context,listen: false).postPost({'key' :'value'});
    if(response.isSuccessful){
      return response.bodyString;
    }else{
      return null;
    }

  }
}

void openOption(BuildContext context, String option) {

}