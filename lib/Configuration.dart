import 'package:flutter/material.dart';
import 'package:nixmix_interface/Status.dart';
import 'package:nixmix_interface/server/NMServer.dart';
import 'package:provider/provider.dart';
import 'main.dart';

class Configuration extends MyApp {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Column(
            children: <Widget>[
              Row(
                  children: [
                    Expanded(
                      child:
                      Padding (
                        padding: const EdgeInsets.only(left: 50.0, top: 50.0, bottom: 50.0, right: 50.0),
                        child:
                          GestureDetector (
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child:
                            Text(
                              'BACK',
                              style: TextStyle(
                                fontSize: 30,
                                letterSpacing: 10,
                                color: Colors.grey,
                              ),
                            ),
                          )
                      ),
                    ),
                    Expanded (
                      child:
                      Column (
                        children: <Widget>[
                          Padding (
                            padding: const EdgeInsets.only(left: 50.0, top: 50.0, bottom: 50.0, right: 50.0),
                            child:
                            Text(
                              'SELECT',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                letterSpacing: 20,
                                fontSize: 40,
                                color: Colors.grey.withOpacity(0.4),
                              ),
                            ),
                          ),
                        ],
                      )
                    )
              ]),
              Spacer(flex: 1),
              Row(
                  children: [
                    Expanded (
                        child:
                        Column (
                          children: <Widget>[
                            Padding (
                              padding: const EdgeInsets.only(left: 50.0, top: 0.0, bottom: 0.0, right: 50.0),
                              child:
                                  GestureDetector (
                                    onTap: () {
                                      openOption(context, "blueNM");
                                    },
                                    child:
                                    Card (
                                        child:
                                        SizedBox (
                                          height: 250.0,
                                          child:
                                          Image.asset(
                                              'assets/images/minerva_transp.png'),
                                        )
                                    ),
                                  ),
                            ),
                            Padding (
                              padding: const EdgeInsets.only(left: 50.0, top: 20.0, bottom: 0.0, right: 50.0),
                              child:
                              Text(
                                'New Mexico Blue Corn',
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ],
                        )
                    ),
                    Expanded (
                        child:
                        Column (
                          children: <Widget>[
                            Padding (
                              padding: const EdgeInsets.only(left: 50.0, top: 0.0, bottom: 0.0, right: 50.0),
                              child:
                              GestureDetector (
                                onTap: () {
                                  openOption(context, "superMasa");
                                },
                                child:
                                Card (
                                    child:
                                    SizedBox (
                                      height: 250.0,
                                      child:
                                      Image.asset(
                                          'assets/images/minerva_transp.png'),
                                    )
                                ),
                              ),
                            ),
                            Padding (
                              padding: const EdgeInsets.only(left: 50.0, top: 20.0, bottom: 0.0, right: 50.0),
                              child:
                              Text(
                                'Super Masa White Corn',
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ],
                        )
                    ),
                    Expanded (
                        child:
                        Column (
                          children: <Widget>[
                            Padding (
                              padding: const EdgeInsets.only(left: 50.0, top: 0.0, bottom: 0.0, right: 50.0),
                              child:
                              GestureDetector (
                                onTap: () {
                                  openOption(context, "textasWhite");
                                },
                                child:
                                Card (
                                    child:
                                    SizedBox (
                                      height: 250.0,
                                      child:
                                      Image.asset(
                                          'assets/images/minerva_transp.png'),
                                    )
                                ),
                              ),
                            ),
                            Padding (
                              padding: const EdgeInsets.only(left: 50.0, top: 20.0, bottom: 0.0, right: 50.0),
                              child:
                              Text(
                                'Texas White Corn',
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ],
                        )
                    ),
                  ]),
              Spacer(flex: 1)
            ],
          ),
      ),
    );
  }

  void onLoading(context) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          elevation: 0,
          backgroundColor: Colors.transparent,
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              new CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
                backgroundColor: Colors.transparent,
              ),
            ],
          ),
        );
      },
    );
  }

}

Future<void> openOption(BuildContext context, String option) async {
  final response = await Provider.of<PostApiService>(context,listen: false).postPost(MyApp.buildBodyJsonResponse("setup"));
  if(response.isSuccessful){
    Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Status()));
  }else{
    MyApp.showAlertDialog(context, "Error", "NixMix Server connection.", "Ok");
  }

}