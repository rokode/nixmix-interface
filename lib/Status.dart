import 'dart:convert';

import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:nixmix_interface/server/NMServer.dart';
import 'package:provider/provider.dart';
import 'Finish.dart';
import 'main.dart';
import 'dart:async';
import 'package:json_annotation/json_annotation.dart';


class Status extends StatefulWidget  {

  @override
  _Status createState() => _Status();

}


class _Status extends State<Status> {
  Duration oneSec = const Duration(seconds:2);
  NixMixState stateData = NixMixState();


  void initState() {
    super.initState();
    new Timer.periodic(oneSec, (Timer t) =>
        updateStatus(context));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: <Widget>[
            Row(
                children: [
                  Expanded (
                      child:
                      Column (
                        children: <Widget>[
                          Padding (
                            padding: const EdgeInsets.only(left: 50.0, top: 50.0, bottom: 25.0, right: 25.0),
                            child:
                            Card (
                                child:
                                Padding(
                                  padding: const EdgeInsets.only(left: 50.0, top: 25.0, bottom: 25.0, right: 50.0),
                                  child:
                                  Column (
                                    children: <Widget>[
                                      Row (
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.only(left: 0.0, top: 20.0, bottom: 20.0, right: 0.0),
                                            child:
                                            Text(
                                              'Step 1:',
                                              style: TextStyle(
                                                fontWeight: FontWeight.normal,
                                                fontSize: 20,
                                                color: Colors.black,
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(left: 20.0, top: 20.0, bottom: 20.0, right: 20.0),
                                            child:
                                            Text(
                                              'Setup',
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 20,
                                                color: Colors.black,
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(left: 0.0, top: 20.0, bottom: 20.0, right: 0.0),
                                            child:
                                            Text(
                                              'Starting',
                                              style: TextStyle(
                                                fontWeight: FontWeight.normal,
                                                fontSize: 20,
                                                color: Colors.black,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row (
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.only(left: 0.0, top: 20.0, bottom: 20.0, right: 0.0),
                                            child:
                                            Text(
                                              'Step 2:',
                                              style: TextStyle(
                                                fontWeight: FontWeight.normal,
                                                fontSize: 20,
                                                color: Colors.black,
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(left: 20.0, top: 20.0, bottom: 20.0, right: 20.0),
                                            child:
                                            Text(
                                              'Cooking',
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 20,
                                                color: Colors.black,
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(left: 0.0, top: 20.0, bottom: 20.0, right: 0.0),
                                            child:
                                            Text(
                                              'Waiting...',
                                              style: TextStyle(
                                                fontWeight: FontWeight.normal,
                                                fontSize: 20,
                                                color: Colors.black,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row (
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.only(left: 0.0, top: 20.0, bottom: 20.0, right: 0.0),
                                            child:
                                            Text(
                                              'Step 3:',
                                              style: TextStyle(
                                                fontWeight: FontWeight.normal,
                                                fontSize: 20,
                                                color: Colors.black,
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(left: 20.0, top: 20.0, bottom: 20.0, right: 20.0),
                                            child:
                                            Text(
                                              'Cleaning',
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 20,
                                                color: Colors.black,
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(left: 0.0, top: 20.0, bottom: 20.0, right: 0.0),
                                            child:
                                            Text(
                                              'Waiting...',
                                              style: TextStyle(
                                                fontWeight: FontWeight.normal,
                                                fontSize: 20,
                                                color: Colors.black,
                                              ),
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                )
                            ),
                          ),
                          GestureDetector (
                            onTap: (){
                              Navigator.pop(context);
                            },
                            child:
                            Padding (
                              padding: const EdgeInsets.only(left: 50.0, top: 10.0, bottom: 10.0, right: 25.0),
                              child:
                              Text(
                                'STOP',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          )
                        ],
                      )
                  ),
                  Expanded (
                      child:
                      Column (
                        children: <Widget>[
                          Padding (
                            padding: const EdgeInsets.only(left: 50.0, top: 50.0, bottom: 50.0, right: 50.0),
                            child:
                            Text(
                              'NIXMIXING',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                letterSpacing: 25,
                                fontSize: 30,
                                color: Colors.grey.withOpacity(0.4),
                              ),
                            ),
                          ),
                          Padding (
                            padding: const EdgeInsets.only(left: 20.0, top: 0.0, bottom: 0.0, right: 25.0),
                            child:
                            Row (
                              children: <Widget>[
                                Card (
                                    child:
                                    Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.only(left: 50.0, top: 50.0, bottom: 20.0, right: 50.0),
                                          child:
                                            GestureDetector(
                                              onDoubleTap: (){
                                                updateStatus(context);
                                              },
                                            child:
                                            Text(
                                              'CURRENT CONDITIONS',
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 25,
                                                color: Colors.black,
                                              ),
                                            ),
                                            ),
                                        ),
                                        Padding(
                                            padding: const EdgeInsets.only(left: 50.0, top: 10.0, bottom: 10.0, right: 50.0),
                                            child:
                                            Row (
                                              children: <Widget>[
                                                Text(
                                                  'Selected option: ',
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.normal,
                                                    fontSize: 20,
                                                    color: Colors.black,
                                                  ),
                                                ),
                                                Text(
                                                  '...',
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 20,
                                                    color: Colors.black,
                                                  ),
                                                ),
                                              ],
                                            )
                                        ),
                                        Padding(
                                            padding: const EdgeInsets.only(left: 50.0, top: 10.0, bottom: 10.0, right: 50.0),
                                            child:
                                            Row (
                                              children: <Widget>[
                                                Text(
                                                  'Temperature: ',
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.normal,
                                                    fontSize: 20,
                                                    color: Colors.black,
                                                  ),
                                                ),
                                                Text(
                                                  stateData.temperature,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 20,
                                                    color: Colors.black,
                                                  ),
                                                ),
                                              ],
                                            )
                                        ),
                                        Padding(
                                            padding: const EdgeInsets.only(left: 50.0, top: 10.0, bottom: 50.0, right: 50.0),
                                            child:
                                            Row (
                                              children: <Widget>[
                                                Text(
                                                  'Water level: ',
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.normal,
                                                    fontSize: 20,
                                                    color: Colors.black,
                                                  ),
                                                ),
                                                Text(
                                                  stateData.waterLevel,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 20,
                                                    color: Colors.black,
                                                  ),
                                                ),
                                              ],
                                            )
                                        ),
                                      ],
                                    )
                                )
                              ],
                            ),
                          ),
                          Padding   (
                              padding: const EdgeInsets.only(left: 25.0, top: 20.0, bottom: 20.0, right: 50.0),
                              child:
                              Row (
                                children: <Widget>[
                                  Text(
                                    'APROX TIME LEFT: ',
                                    style: TextStyle(
                                      fontWeight: FontWeight.normal,
                                      fontSize: 20,
                                      color: Colors.black,
                                    ),
                                  ),
                                  GestureDetector(
                                    onDoubleTap: (){
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(builder: (context) => Finish()),);
                                      },
                                    child:
                                    Text(
                                      '00:00:00',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20,
                                        color: Colors.black,
                                      ),
                                    ),
                                  )
                                ],
                              )
                          ),
                        ],
                      )
                  )
                ]),
            Spacer(flex: 1)
          ],
        ),
      ),
    );
  }
  void onLoading(context) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          elevation: 0,
          backgroundColor: Colors.transparent,
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              new CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
                backgroundColor: Colors.transparent,
              ),
            ],
          ),
        );
      },
    );
  }
  void updateContent(NixMixState currentStateData){
    stateData.phLevel = currentStateData.phLevel;
    stateData.selectedOption = currentStateData.selectedOption;
    stateData.temperature = currentStateData.temperature;
    stateData.waterLevel = currentStateData.waterLevel;
    setState(() {
      stateData.phLevel = currentStateData.phLevel;
      stateData.selectedOption = currentStateData.selectedOption;
      stateData.temperature = currentStateData.temperature;
      stateData.waterLevel = currentStateData.waterLevel;
    });
  }

  Future<void> updateStatus(context) async {
    final response = await Provider.of<PostApiService>(context,listen: false).getConditions();
    if(response != null){
      if(response.body!=null){
        NixMixState currentStateData = NixMixState();
        currentStateData.phLevel = response.body["phLevel"];
        currentStateData.selectedOption = response.body['selectedOption'];
        currentStateData.temperature = response.body['temperature'];
        currentStateData.waterLevel = response.body['waterLevel'];
        updateContent(currentStateData);
      }
    }
    return;
  }

}

class NixMixState {
  String phLevel = "7pH";
  String selectedOption = "...";
  String temperature = "0F";
  String waterLevel = "0Gal";
}
